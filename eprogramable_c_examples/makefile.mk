########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejercicio 1:
#PROYECTO_ACTIVO = ejercicio1
#NOMBRE_EJECUTABLE = ejercicio1.exe

####Ejercicio 2:
#PROYECTO_ACTIVO = ejercicio2
#NOMBRE_EJECUTABLE = ejercicio2.exe

####Ejercicio 3:
PROYECTO_ACTIVO = ejercicio3
NOMBRE_EJECUTABLE = ejercicio3.exe




####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe
