/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es: Cantarutti Maximiliano
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*==================[inclusions]=============================================*/
#include "main.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

void BinaryToBcd(uint32_t data,uint8_t digits,uint8_t *bcd_ptr)
{
	uint8_t i=0;
	for (i=0;i<digits;i++)
	{
		*bcd_ptr = data%10;//Divido por 10 para separar los decimales, guardo el resto en el puntero
		data = data/10;

		printf(" %d \r\n",*bcd_ptr);//muestra el resto por pantalla (1er digito)
		bcd_ptr++;

	}

}

int main(void)
{
   	uint8_t bcd[10], *bcd_ptr, digitos;
	uint32_t data;
	bcd_ptr=bcd;

	//printf("Introduce un número: ");
	//scanf( "%" SCNu32,&data);//
	data=134;
	printf("Ingreso: %" PRIu32 "\n", data);
	//printf("Introduce el número de dígitos: ");
	//scanf( "%" SCNu8,&digitos);
	digitos = 3;
	printf("Ingreso: %" PRIu8 "\n", digitos);
	printf("los dígitos son: \n");

	BinaryToBcd(data,digitos,bcd_ptr);
	Sleep(10);


	return 0;
}

/*==================[end of file]============================================*/

