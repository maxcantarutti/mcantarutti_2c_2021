/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"

/*==================[macros and definitions]=================================*/
//defino el struct como está en la guia
typedef struct
{
	uint8_t port;				/*!< GPIO port number */
	uint8_t pin;				/*!< GPIO pin number */
	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;

/*==================[internal functions declaration]=========================*/

void PortControl(uint8_t bcd, gpioConf_t *puntero_puerto)
{
	uint8_t i=0;

	for(i=0;i<4;i++)
		{
		printf("El puerto %d.%d ",puntero_puerto->port,puntero_puerto->pin); //valor entero "." valor decimal

			if(bcd%2==1)
					{printf(" Está en alto \r\n");}

					else{printf(" Está en bajo \r\n");}

	bcd=bcd/2;
	puntero_puerto++;
		}
}

int main(void)
{
	uint8_t bcd;
   	gpioConf_t puertos[4],*puntero_puerto;
	puertos[0].port=1;
	puertos[1].port=1;
	puertos[2].port=1;
	puertos[3].port=2;
	puertos[0].pin=4;
	puertos[1].pin=5;
	puertos[2].pin=6;
	puertos[3].pin=14;
	puntero_puerto=puertos;

   	//printf("Introduce un número de un dígito: ");
   	bcd=8; //modificar aca el valor
   	//scanf( "%" SCNu8,&bcd); No funciona la entrada por consola!
   	//printf("Ingreso: %" PRIu8 "\n",bcd);

   	PortControl(bcd,puntero_puerto);

	Sleep(10); //Sleep va con mayusculas

	return 0;
}

/*==================[end of file]============================================*/

