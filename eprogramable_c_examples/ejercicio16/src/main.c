/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/* a)Declare una variable sin signo de 32 bits y cargue el valor 0x01020304.
 * Declare cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y truncamiento,
 * cargue cada uno de los bytes de la variable de 32 bits.
 * b)Realice el mismo ejercicio, utilizando la definición de una “union”.
 *
 */
/*==================[inclusions]=============================================*/
#include "main.h"



/*==================[macros and definitions]=================================*/

  uint32_t VARIABLE_32 =0x01020304;
  uint8_t VARIABLE_0=0;
  uint8_t VARIABLE_1=1;
  uint8_t VARIABLE_2=2;
  uint8_t VARIABLE_3=3;


  uint32_t VARIABLE_32b =0x01020304;
    uint8_t VARIABLE_0b=0;
    uint8_t VARIABLE_1b=1;
    uint8_t VARIABLE_2b=2;
    uint8_t VARIABLE_3b=3;
/*==================[internal functions declaration]=========================*/

int main(void)
{
	VARIABLE_0 = VARIABLE_32;
	VARIABLE_1 = (VARIABLE_32>>8);
	VARIABLE_2 = (VARIABLE_32>>16);
	VARIABLE_3 = (VARIABLE_32>>24);
/*----------------------------------------*/
/* Segundo método */
	union VARIABLES{
				struct	{
					uint8_t VARIABLE_0b;
					uint8_t VARIABLE_1b;
					uint8_t VARIABLE_2b;
					uint8_t VARIABLE_3b;
						}B;
				uint32_t  VARIABLE32_b;

						}U;
	printf("%d\r\n", VARIABLE_0);
	printf("%d\r\n", VARIABLE_1);
	printf("%d\r\n", VARIABLE_2);
	printf("%d\r\n", VARIABLE_3);
	printf("%d\r\n", VARIABLE_32);


	printf("%d\r\n", VARIABLE_0b);
	printf("%d\r\n", VARIABLE_1b);
	printf("%d\r\n", VARIABLE_2b);
	printf("%d\r\n", VARIABLE_3b);
	printf("%d\r\n", U.VARIABLE32_b);
	return 0;
}

/*==================[end of file]============================================*/
/*
*/
