/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es: CANTARUTTI MAXIMILIANO
 * 
 *FIJARSE EL COMPORTAMIENTO, NO ESTÁ EJECUTANDO PRIMERO LAS OPCIONES SI NO LOS INGRESOS!!
 *VER EL TEMA DE LOS SCNu8 DE LOS SCANF
 */

/*==================[inclusions]=============================================*/
#include "main.h"

/*==================[macros and definitions]=================================*/
typedef struct
{
	uint8_t numero_led;       //indica el número de led a controlar
	uint8_t numero_ciclos;   //indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;    //indica el tiempo de cada ciclo
	uint8_t modo;       //ON 1, OFF 0, TOGGLE 2
}MI_LED;

/*==================[internal functions declaration]=========================*/
void Retardo(uint8_t periodo)
{
 Sleep(periodo);
};
void PrendeLed(uint8_t led)
{
	printf("Se prende el led %d \r\n",led);
};

void ApagaLed(uint8_t led)
{
	printf("Se apaga el led %d \r\n",led);

};

void Toggle(uint8_t led,uint8_t ciclos,uint8_t periodo)
{
	uint8_t j=0;
	while(j<ciclos)
	{
		PrendeLed(led);
		Retardo(periodo);
		ApagaLed(led);
		Retardo(periodo);
		j++;
	}

};


void ActualLed(MI_LED *led_ptr)
{

	switch(led_ptr->modo)
	{
		case(0):
		ApagaLed(led_ptr->numero_led);
			break;
		case(1):
			PrendeLed(led_ptr->numero_led);
			break;
		case(2):
		Toggle(led_ptr->numero_led,led_ptr->numero_ciclos,led_ptr->periodo);
			break;
	}
}

int main(void)
{
	MI_LED prueba, *led_ptr;
	led_ptr=&prueba;
	printf("Introduce un modo: \r\n");
	printf("Modo 0: apaga led \r\n");
	printf("Modo 1: prende led \r\n");
	printf("Modo 2: modo toogle \r\n");
	//scanf("%"SCNu8, &prueba.modo); //SCNu8--> uint8_t, de otra forma no acepta. Porque error de sintaxis??
	prueba.modo=2;
	//printf("Ingrese el número de led: \n");
	//scanf( "%"SCNu8, &prueba.numero_led);
	prueba.numero_led=2;
	if(prueba.modo==2)
	{

		//printf("Introduce número el ciclo: \n");
		//scanf( "%"SCNu8,&prueba.numero_ciclos);
		prueba.numero_ciclos = 15;
		//printf("Introduce el periodo: \n");
		//scanf( "%"SCNu8,&prueba.periodo);
		prueba.periodo = 12;

	}

	ActualLed(led_ptr);

	Sleep(1);
	return(0);
}
/*==================[end of file]============================================*/

