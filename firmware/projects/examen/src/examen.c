/*! @mainpage EXAMEN 1.0
 *
 * \section genDesc General Description
 *
 * This application is about a radar, with lectures within 75 grades. This application acuses the most close objet in range
 *
 * \section hardConn Hardware Connection
 *
 * |    HC-SR4     	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	T_FIL2		|
 * | 	TRIGGER	 	| 	T_FIL3		|
 * | 	+5V 	 	| 	+5V 		|
 * | 	GND 	 	| 	GND 		|
 *
 * | POTENCIOMETRO	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT		 	| 	CH1			|
 * | 	VCC		 	| 	3,5v		|
 * | 	GND		 	| 	GND			|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/11/2021 | Document creation		                         |
 * | 01/11/2021 | Document complete		                         |
 *
 * @author Maximiliano Cantarutti
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/examen.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "hc_sr4.h"
#include "gpio.h"
#include "goniometro.h"
#include "delay.h"
#include "timer.h" /*para proyecto 3*/
#include "uart.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
bool TEC1_FLG =false;
bool MOTOR_FLG =false;
int16_t dato;
uint16_t motor_state;
uint16_t motor1_2 = 300; //EL VALOR 300 ES UNA SUPOSICÓN, ACA VA EL VALOR MEDIDO DEL POTENCIOMETRO A 1,2V
uint16_t motor2_1 = 600;//EL VALOR 600 ES UNA SUPOSICÓN, ACA VA EL VALOR MEDIDO DEL POTENCIOMETRO A 2,1 V

uint16_t distancia=0;
uint16_t angulo_final;
uint16_t distancia_minima = 500;
uint16_t valores_angulos[6]= {0, 15, 30, 45, 60, 75};
/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/

bool HcSr04Init(gpio_t echo, gpio_t trigger);
int16_t HcSr04ReadDistanceCentimeters();
void tecla_1(){TEC1_FLG = !TEC1_FLG;};

void direccion_motor(){MOTOR_FLG = !MOTOR_FLG;}; //INVIERTE LA DIRECCIÓN DEL MOTOR

/*==================[external data definition]===============================*/



void Tiempo()
{
	/* seccion control motor y leds*/
	motor_state = PoteValue(); //pregunto en que valor se encuentra el potenciometro
	 if (motor_state == motor1_2){
		direccion_motor();
		LedOn(LED_RGB_G);}
		else if (motor_state == motor2_1){
			direccion_motor();
			LedOn(LED_RGB_R);}
/*Sección medicion*/

	if (TEC1_FLG){
		 AnalogStartConvertion();
		 DelayMs(1);
		 angulo_final = ReadDataAndConvertion(); //Leo el angulo y lo convierto
		 for (int i=0; i<5; i++) //Comparo con la base de datos de ángulos
		 				{
			 if(angulo_final==valores_angulos[i]){
		 		distancia = HcSr04ReadDistanceCentimeters(); /*Si coincide con un ángulo llama a la funcion para leer disancia*/
		 		UartSendString(SERIAL_PORT_PC,UartItoa(angulo_final, 10)); //muestro angulo y distancia
		 		UartSendString(SERIAL_PORT_PC," ° objeto a: ");
		 		UartSendString(SERIAL_PORT_PC,UartItoa(distancia, 10));
		 		UartSendString(SERIAL_PORT_PC," cm\r\n");
		 			if (distancia_minima > distancia) //si la distancia es menor a la anterior guardo la nueva
		 				distancia_minima = distancia;

		 			if(angulo_final == 0 || angulo_final==75)
		 												{ //acuso la menor distancia si se alcanza algun extremo
		 				UartSendString(SERIAL_PORT_PC," Distancia mas corta: ");
		 				UartSendString(SERIAL_PORT_PC,UartItoa(distancia_minima, 10));
		 				UartSendString(SERIAL_PORT_PC," cm\r\n");}
		 												}
		 				}//fin del for
				}//fin del primer if

	     distancia=0;


};

void doUart(){ //controlo la toma de datos ingresando "A"

    UartReadByte(SERIAL_PORT_PC,&dato);
	switch (dato){
			case 'A':
			{
			TEC1_FLG = !TEC1_FLG;

			break;
			}

			default:
			UartSendString(SERIAL_PORT_PC,"Error");

	}

};

/*==================[external functions definition]==========================*/

int main(void){
	SystemClockInit();

		/*Inicializacion del display, leds, switches y sensor*/
		LedsInit();

		/*configuración ultrasonido*/
		HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);
		SwitchActivInt(SWITCH_1, &tecla_1); //control con una tecla de inicio

		timer_config my_timer = {TIMER_A,1000,&Tiempo};
		TimerInit(&my_timer);
		TimerStart(TIMER_A);
		serial_config my_uart = {SERIAL_PORT_PC,10000,&doUart};
		UartInit(&my_uart);



    while(1){

	}
    
	return 0;
}



/*==================[end of file]============================================*/

