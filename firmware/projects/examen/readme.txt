﻿Descripción del Proyecto.
Este examen consta de un medidor de distancia de ultrasonido. Guarda la distancia del objeto más cercano dentro de los 6 rangos de apertura. Además, prende una luz distintiva para cada dirección

La conexión es:
Para el potenciometro: La salida OUT (la del medio) debe ir conectado a la entrada analógica CH1 de la EDU-CIAA. VCC a 3,5 y GND a GND
Para el sensor de ultrasonido: La entrada ECHO debe ir conectada a T_FIL2 de la EDU-CIAA.
La salida TRIGGER debe conectarse a T_FIL3. VCC a +5V y GND a GND