/*! @mainpage Recuperatorio
 *
 * \section genDesc General Description
 *
 * Esta aplicación controla un sistema de cintas transportadoras para el llenado y control de cajas.
 *
 * \section hardConn Hardware Connection
 *
 * | 	BALANZA		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	5V			|
 * | 	GND		 	| 	GND			|
 * | 	OUT		 	| 	CH1			|
 *
 * | 	Tcrt5000	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	3.3			|
 * | 	GND		 	| 	GND			|
 * | 	OUT		 	| 	T_COL0		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 11/1/2021  | Document creation		                         |
 * | 11/1/2021 	| Document complete	                             |
 *
 * @author Maximiliano Cantarutti
 *
 */

/*==================[inclusions]=============================================*/

#include "recuperatorio.h"       /* <= own header */
#include "Tcrt5000.h"
#include "timer.h"
#include "uart.h"
#include "led.h"
#include "gpio.h"
#include "balanza.h"
#include "systemclock.h"
#include "switch.h"
/*==================[macros and definitions]=================================*/
gpio_t pin_sensor=GPIO_T_COL0; // pin de entrada al sensor
bool TEC1_FLG =false; //comienzo apagado
bool TEC2_FLG =false;
bool estado_pin=false;
uint16_t peso;
uint16_t dato;
uint16_t contador_caja =0;
uint16_t contador_tiempo =0;
uint16_t tiempo_llenado_max =0;
uint16_t tiempo_llenado_min =10000; //Un umbral para asegurarse de que el primer valor leído pase a ser el mínimo momentáneamente

/*==================[internal data definition]===============================*/

void tecla_1(){TEC1_FLG = !TEC1_FLG;};
void tecla_2(){TEC2_FLG = !TEC2_FLG;};

/*==================[internal functions declaration]=========================*/
void Tiempo();
void doUart();

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
/* La funcion Tiempo será la encargada de llevar a cabo el manejo de las cintas y demás por interrupciones.  */
void Tiempo(){
	if (TEC1_FLG){ //tec1 boton de inicio o detención. Si es true, se inicia el sistema.
		estado_pin=Tcrt5000State();//lee el estado del pin
		while (estado_pin==false) //no se lee el lugar de llenado
		{LedOn(LED_1);
		LedOff(LED_2);} //LED 1 indica que la cinta 1 avanza y LED 2 apagado cinta 2 apagada

		//si no se cumple estado_pin==false se sale del while y se sigue:
		while (estado_pin==true){
			LedOff(LED_1); //se detiene cinta 1
			LedOn(LED_2); //LED 2 indica que la cinta 2 avanza
			DatoPeso(&peso);
			while (peso < 20.1){
				//la cinta 2 esta avanzando, llenando la caja
				DelayMs(25); //para tener una variable para contar, voy sumando de a 1ms el contador_tiempo (tiempo más que suficiente para la velocidad de la cinta)
				contador_tiempo+=25; //cuenta cada 25 segundos (ciclo del refresco del timer)
				DatoPeso(&peso); //se sigue llenando la caja hasta los 20Kg
								}//fin del while, cuando llega a 20 se sale del mismo y se suma la caja

				contador_caja++;
				UartSendString(SERIAL_PORT_PC," TIEMPO DE LLENADO DE CAJA "); //se acusa los datos
				UartSendString(SERIAL_PORT_PC,UartItoa(contador_caja, 10));
				UartSendString(SERIAL_PORT_PC," EN: ");
				UartSendString(SERIAL_PORT_PC,UartItoa(contador_tiempo, 10));
				UartSendString(SERIAL_PORT_PC," ms\r\n");
					if(tiempo_llenado_max < contador_tiempo)//Si el contador de tiempo supera el maximo, es el nuevo maximo
						{tiempo_llenado_max = contador_tiempo;}
					else if(tiempo_llenado_min>contador_tiempo) //si el contador de tiempo no supera al mininmo es el nuevo minimo
						{tiempo_llenado_min = contador_tiempo;}
				contador_tiempo=0; //reseteo tiempo pero no numero de cajas
				estado_pin==false; //vuelvo el estado del pin a la configuracion inicial para que arranque la cinta 1 de nuevo y repito CREO QUE NO ES NECESARIA ESTA LINEA

				if (contador_caja == 15){//ahora la condicion de llenado de lote
					UartSendString(SERIAL_PORT_PC," TIEMPO DE LLENADO MAXIMO: "); //se acusa los datos
					UartSendString(SERIAL_PORT_PC,UartItoa(tiempo_llenado_max, 10));
					UartSendString(SERIAL_PORT_PC," ms\r\n");
					UartSendString(SERIAL_PORT_PC," TIEMPO DE LLENADO MINIMO: ");
					UartSendString(SERIAL_PORT_PC,UartItoa(tiempo_llenado_min, 10));
					UartSendString(SERIAL_PORT_PC," ms\r\n");
					contador_caja = 0; //se reinicia el lote
					tiempo_llenado_max =0;
					tiempo_llenado_min =10000;
									}//fin del ultimo if

		}//fin del while de estado_pin

	}//fin del if general.
	else if(TEC2_FLG){
		TEC1_FLG = false;
		Tcrt5000Deinit(pin_sensor); //detengo el sensor
		LedOff(LED_1);
		LedOff(LED_2);
		LedOff(LED_3);

	}
}



void doUart(){
    UartReadByte(SERIAL_PORT_PC,&dato);

	switch (dato){
			case 'S'://Comenzar
			{tecla_1();
		    LedOn(LED_3); }//led de encendido

			break;
			case 'A'://Detener
			{tecla_2();
			LedOff(LED_3);} //led de encendido

			break;
			default:
			UartSendString(SERIAL_PORT_PC,"Error ingreso de comando");

			}


};
int main(void){
	SystemClockInit();
	//Se inicializa los leds:
	gpio_t pin_leds[4]={GPIO_LED_3,GPIO_LED_2,GPIO_LED_1,GPIO_LED_RGB_B};
	LedsInit();

	/*Se inicializa el sensor infrarrojo*/
	Tcrt5000Init(pin_sensor);// inicializo el pin como entrada

	/*Se inicializa la UART*/
	serial_config my_uart = {SERIAL_PORT_PC,115200,&doUart};
	UartInit(&my_uart);

	/*Se inicializa el switch*/
	SwitchActivInt(SWITCH_1, &tecla_1);
	SwitchActivInt(SWITCH_2, &tecla_2);

	/*Se inicializa el timer*/
	timer_config my_timer = {TIMER_A,25,&Tiempo}; //a 25 ms
	TimerInit(&my_timer);
	TimerStart(TIMER_A);

    while(1){

	}
    
	return 0;
}

/*==================[end of file]============================================*/

