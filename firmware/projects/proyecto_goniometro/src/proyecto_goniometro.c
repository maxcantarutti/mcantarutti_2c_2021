/*! @mainpage GONIOMETRO INALAMBRICO 1.0
 *
 * \section genDesc General Description
 *
 * Este software permite la obtención en tiempo real, y con una tasa de refresco de 50ms, el ángulo de la rodilla. El mismo es mostrado en la pantalla LCD, y a su vez es
 * procesado y mostrado, via bluetooth, en un dispositivo móvil.
 * Adicionalmente, puede mostrar una ventana de tiempo que encierre la duración de un ejercicio en particular, por ejemplo, una sentadilla.
 * Como post procesamiento, provee información acerca de la velocidad angular y aceleración angular de la rodilla, así como una gráfica acotada en el tiempo.
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * |  POTENTIOMETER	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	+3,3V		|
 * | 	OUT		 	| 	CH1			|
 * | 	GND		 	| 	GND			|
 *
 *
 * |    HC_05		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	+5V			|
 * | 	RXD		 	| 	232_TX		|
 * | 	TXD		 	| 	232_RX		|
 * | 	GND		 	| 	GND			|
 *
 *
 * |   ITS_E0803   	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	D1	    	| 	LCD1		|
 * | 	D2	    	| 	LCD2		|
 * | 	D3 	  	    | 	LCD3		|
 * | 	D4  	 	| 	LCD4 		|
 * | 	SEL_0 	 	| 	GPIO1 		|
 * | 	SEL_1 	 	| 	GPIO3 		|
 * | 	SEL_2 	 	| 	GPIO5 		|
 * | 	+5v 	 	| 	+5V 		|
 * | 	GND 	 	| 	GND 		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    						|
 * |:----------:|:---------------------------------------------------------------------:|
 * | 07/10/2021 | Document creation		                         						|
 * | 07/10/2021	| Potentiometer funtionallity chequed            						|
 * | 14/10/2021	| Added a Display. The potentiometer's value is now shown in display 	|
 * | 14/10/2021	| Converter Function added. Now the data is shown in centigrades		|
 * | 21/10/2021	| Bluetooth connection added. Now the data is plotted in an android app	|
 * | 28/10/2021	| 7805 regulator added. Velocity and aceleration data added 			|
 * | 04/11/2021	| Aceleration functions added. Document complete :)						|
 *
 * @author Cantarutti Maximiliano Javier
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/proyecto_goniometro.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
//#include "gpio.h"
#include "delay.h"
#include "timer.h" /*para proyecto 3*/
#include "uart.h"
#include "analog_io.h"
#include "DisplayITS_E0803.h"
#include "HC_05.h"
/*==================[macros and definitions]=================================*/
uint16_t valor_pote;
bool TEC1_FLG =false;
uint16_t dato;
uint16_t clear;

uint16_t angulo_maximo = 180;
uint16_t valor_pote_a180 =623 ; //MEDIR A QUE VALOR CORRESPONDEN 180°
uint16_t angulo;
uint16_t angulo_final;
float delta_t = 0.05;
float dif;
float diff;
uint16_t diferencial;
uint16_t vector[]={0,0,0};
uint16_t vectorac[]={0,0};
uint16_t ang_ini;
uint16_t ang_fin;
uint16_t vel_ini;
uint16_t vel_fin;
uint16_t iter=0;
uint16_t iterac=0;
uint16_t decimal;
uint16_t parte_entera;


/*==================[internal data definition]===============================*/


/*==================[internal functions declaration]=========================*/
void tecla_1(){TEC1_FLG = !TEC1_FLG; LedOn(LED_RGB_G);};
uint16_t ConvertOhmToGrades(uint16_t valor_pote);

/*==================[external data definition]===============================*/
void Tiempo(){
	if (TEC1_FLG){

			AnalogStartConvertion();
			DelayMs(1);
			AnalogInputRead(CH1, &valor_pote);
			angulo_final = ConvertOhmToGrades(valor_pote);
			//Sale por Hterm
			UartSendString(SERIAL_PORT_PC,UartItoa(angulo_final, 10));

			//se manda por bluetooth:
			HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)"*G");
			HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)UartItoa(angulo_final, 10));
			HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)"*");

			HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)"*A");
			HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)UartItoa(angulo_final, 10));
			HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)"     *");
			//HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)"*AC");


			ITSE0803DisplayValue(angulo_final); //muestra el angulo por pantalla
			//UartSendString(SERIAL_PORT_PC," grados\r\n");

			//VELOCIDAD
			if (iter <3)
			{
				vector[iter]=angulo_final;
				iter++;

			}
			else {
				ang_ini=vector[0];
				ang_fin=vector[2];
				Diff(ang_ini,ang_fin,&dif);

				HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)"*D");
				HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)UartItoa(dif, 10));
				HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)".");
				HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)UartItoa( (dif-(float)(uint16_t)dif) *100, 10 ));
				HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)"     *");
				UartSendString(SERIAL_PORT_PC,UartItoa(dif, 10));
				iter=0;
				//ACELERACION
					if(iterac<2)
					{
						vectorac[iterac]=dif;
						iterac++;
					}
						else{
							vel_ini = vectorac[0];
							vel_fin = vectorac[1];
							Diff(vel_ini,vel_fin,&diff);
							HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)"*S");
							HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)UartItoa(diff, 10));
							HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)".");
							HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)UartItoa( (dif-(float)(uint16_t)diff) *100, 10 ));
							HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)"     *");
							UartSendString(SERIAL_PORT_PC,UartItoa(diff, 10));
							iterac=0;
							vectorac[0]=0;
							vectorac[1]=0;
						}

				}




	}
}

void doUart(){

    UartReadByte(SERIAL_PORT_P2_CONNECTOR,&dato);
    UartReadByte(SERIAL_PORT_P2_CONNECTOR,&clear);

    UartReadByte(SERIAL_PORT_PC,&dato);

	switch (dato){
			case 'I'://PAUSA-CONTINUAR
			{TEC1_FLG = !TEC1_FLG;}
		    LedToggle(LED_1);

			break;
			case 'C': //LIMPIA PANTALLA, ENCIENDE, APAGA.
			{
			HC05SendString(SERIAL_PORT_P2_CONNECTOR,(uint8_t *)"*GC");
			TEC1_FLG = !TEC1_FLG;
		    LedOn(LED_RGB_G);};
			break;

			default:
			UartSendString(SERIAL_PORT_PC,"Error");

			}


};
/*La función que sigue, realiza la conversión del valor dado por el pote, a grados decimales*/
/*Rango pote 1023 a 0. */
uint16_t ConvertOhmToGrades(uint16_t valor_pote){

	angulo = (valor_pote * angulo_maximo)/valor_pote_a180;
    angulo=180-angulo;
return angulo;
}

void Diff(uint16_t ang_i, uint16_t ang_f, float *dif){
*dif =((float)(ang_f-ang_i)/delta_t);
*dif = *dif * (3.141516/180);

//return dif;
}

/*==================[external functions definition]==========================*/

int main(void){
        SystemClockInit();
		SwitchesInit();
		LedsInit();
		SwitchActivInt(SWITCH_1, &tecla_1);
		analog_input_config my_io = {CH1,AINPUTS_SINGLE_READ,NULL};
		AnalogInputInit(&my_io);

		serial_config my_uart = {SERIAL_PORT_PC,115200,&doUart};
    	UartInit(&my_uart);
    	//Modulo bluetooth
    	serial_config my_uartb = {SERIAL_PORT_P2_CONNECTOR, 9600, &doUart};

    	//UartInit(&my_uartb);
    	HC05Init(&my_uartb);

    	timer_config my_timer = {TIMER_A,50,&Tiempo};
    	TimerInit(&my_timer);
    	TimerStart(TIMER_A);
    	//UartSendString(SERIAL_PORT_PC,"linea de prueba");

    	/*configuración LCD*/
    	gpio_t pines_config[7] = {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
    	gpio_t *ptr_pines_conf = pines_config;

    	/*Inicializacion del display, leds, switches y sensor*/
    	ITSE0803Init(ptr_pines_conf);


    while(1){

	}
    
	return 0;
}

/*==================[end of file]============================================*/

