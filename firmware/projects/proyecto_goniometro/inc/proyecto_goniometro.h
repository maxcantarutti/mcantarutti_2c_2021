/*! @mainpage GONIOMETRO INALAMBRICO 1.0
 *
 * \section genDesc General Description
 *
 * Este software permite la obtención en tiempo real, y con una tasa de refresco de 50ms, el ángulo de la rodilla. El mismo es mostrado en la pantalla LCD, y a su vez es
 * procesado y mostrado, via bluetooth, en un dispositivo móvil.
 * Adicionalmente, puede mostrar una ventana de tiempo que encierre la duración de un ejercicio en particular, por ejemplo, una sentadilla.
 * Como post procesamiento, provee información acerca de la velocidad angular y aceleración angular de la rodilla, así como una gráfica acotada en el tiempo.
 *
 *
 *
 * \section hardConn Hardware Connection
 *
 * |  POTENTIOMETER	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	+3,3V		|
 * | 	OUT		 	| 	CH1			|
 * | 	GND		 	| 	GND			|
 *
 *
 * |    HC_05		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	+5V			|
 * | 	RXD		 	| 	232_TX		|
 * | 	TXD		 	| 	232_RX		|
 * | 	GND		 	| 	GND			|
 *
 *
 * |   ITS_E0803   	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	D1	    	| 	LCD1		|
 * | 	D2	    	| 	LCD2		|
 * | 	D3 	  	    | 	LCD3		|
 * | 	D4  	 	| 	LCD4 		|
 * | 	SEL_0 	 	| 	GPIO1 		|
 * | 	SEL_1 	 	| 	GPIO3 		|
 * | 	SEL_2 	 	| 	GPIO5 		|
 * | 	+5v 	 	| 	+5V 		|
 * | 	GND 	 	| 	GND 		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    						|
 * |:----------:|:---------------------------------------------------------------------:|
 * | 07/10/2021 | Document creation		                         						|
 * | 07/10/2021	| Potentiometer funtionallity chequed            						|
 * | 14/10/2021	| Added a Display. The potentiometer's value is now shown in display 	|
 * | 14/10/2021	| Converter Function added. Now the data is shown in centigrades		|
 * | 21/10/2021	| Bluetooth connection added. Now the data is plotted in an android app	|
 * | 28/10/2021	| 7805 regulator added. Velocity and aceleration data added 			|
 * | 04/11/2021	| Aceleration functions added. Document complete :)						|
 *
 * @author Cantarutti Maximiliano Javier
 *
 */

#ifndef _PROYECTO_GONIOMETRO_H
#define _PROYECTO_GONIOMETRO_H


/*==================[inclusions]=============================================*/
#include "stdint.h"
#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/
void Diff(uint16_t ang_i, uint16_t ang_f, float *dif);
uint16_t ConvertOhmToGrades(uint16_t valor_pote);
void tecla_1();

/*==================[end of file]============================================*/


#endif

