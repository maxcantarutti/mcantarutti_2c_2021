/*! @mainpage ULTRASOUND METER (SERIAL PORT) 2.0
 * This appliaction shows the distance (in centimeters) between an objet and the sensor, in a display. This uses two devices, an ultrasound meter and a display. Also, the data is shown by HTerm software on PC
 *
 *
 * \section genDesc General Description
 *
 * This application makes the ultrasound meter
 *
 * \section hardConn Hardware Connection
 *
 * |    HC-SR4     	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	T_FIL2		|
 * | 	TRIGGER	 	| 	T_FIL3		|
 * | 	+5V 	 	| 	+5V 		|
 * | 	GND 	 	| 	GND 		|

 *
 * |   ITS_E0803   	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	D1	    	| 	LCD1		|
 * | 	D2	    	| 	LCD2		|
 * | 	D3 	  	    | 	LCD3		|
 * | 	D4  	 	| 	LCD4 		|
 * | 	SEL_0 	 	| 	GPIO1 		|
 * | 	SEL_1 	 	| 	GPIO3 		|
 * | 	SEL_2 	 	| 	GPIO5 		|
 * | 	+5v 	 	| 	+5V 		|
 * | 	GND 	 	| 	GND 		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 1/09/2021  | Document creation		                         |
 * | 3/09/2021  | Ultrasound device configurated  and tested     |
 * | 10/09/2021 | New display configuration added and tested     |
 * | 24/09/2021 | Added interruptions lines (tested)             |
 * | 24/09/2021 | Added Serial Port instructions                 |
 * | 24/09/2021 | Completed                                      |
 *
 * @author Maximiliano Javier Cantarutti
 *
 */

/*==================[inclusions]=============================================*/
#include "../../ultrasound_meter_wi_serial_port/inc/ultrasound_meter_wi_serial_port.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "hc_sr4.h"
#include "gpio.h"
#include "delay.h"
#include "timer.h" /*para proyecto 3*/
#include "uart.h"
/*==================[macros and definitions]=================================*/
bool TEC1_FLG =false;
bool TEC2_FLG=false;
uint8_t teclas;
int16_t distancia;
int16_t dato;


/*==================[internal data definition]===============================*/
int16_t HcSr04ReadDistanceCentimeters();
/* La funcion HcSr04ReadDistanceCentimeters, cuando es llamada, sensa el dispositivo ultrasonido y acumula e microsegundos el tiempo
 * en que el mismo devuelve una respuesta en alto. No recibe parámetro alguno y devuelve un entero, que es la distancia en cm *
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger);
/*La funcion HcSr04Init se encarga de asignar los pines para la lectura del módulo ultrasonido.
 * Siendo echo el impulso recibido por la CIAA que significa la detección de un elemento, y trigger el disparo.
 * Recibe como parametro dos variables tipo gpio_t y devuelve 1 y 0.*/

/*==================[internal functions declaration]=========================*/
void tecla_1(){TEC1_FLG = !TEC1_FLG;};

void tecla_2(){TEC2_FLG = !TEC2_FLG;};


/*==================[external data definition]===============================*/

void Tiempo()
{
	 if (TEC1_FLG){

	    	 distancia = HcSr04ReadDistanceCentimeters(); /*Si se solicita, llama a la funcion para leer disancia*/

	    	if(TEC2_FLG)
	    	{
	    		if(distancia<10) {
	      	  LedOn(LED_RGB_B);
	      		      LedOff(LED_2);
	      	          LedOff(LED_1);
	      	          LedOff(LED_3);
	           }
	       else  {
	       if(distancia<20)
	      	  {LedOn(LED_RGB_B);
	         	  LedOn(LED_1);
	         	  LedOff(LED_2);
	         	  LedOff(LED_3);}
	      	  else{
	              if(distancia<30) {
	            LedOn(LED_2);
	  		  LedOn(LED_RGB_B);
	  		  LedOn(LED_1);
	  	      LedOff(LED_3);}
	            else {
	          	  LedOn(LED_2);
	          	  LedOn(LED_RGB_B);
	          	  LedOn(LED_1);
	          	  LedOn(LED_3);
	            }}}
	    	ITSE0803DisplayValue(distancia); /*devuelve el valor de la distancia en cm*/
	    	UartSendString(SERIAL_PORT_PC,UartItoa(distancia, 10));
	    	UartSendString(SERIAL_PORT_PC," cm\r\n");
	    	}
	     }
	     else {
	    	 ITSE0803DisplayValue(0);
	          LedOff(LED_RGB_B);
	          LedOff(LED_2);
	          LedOff(LED_1);
	          LedOff(LED_3);
	     }
	     DelaySec(1);
	     distancia=0;


};

void doUart(){

    UartReadByte(SERIAL_PORT_PC,&dato);
	switch (dato){
			case 'O':
			{
			TEC1_FLG = !TEC1_FLG;

			break;
			}
			case 'H':
			{
			TEC2_FLG = !TEC2_FLG;

			break;
			}
			default:
			UartSendString(SERIAL_PORT_PC,"Error");

	}

};




/*==================[external functions definition]==========================*/

int main(void)
{
	SystemClockInit();

	/*configuración LCD*/
	gpio_t pines_config[7] = {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
	gpio_t *ptr_pines_conf = pines_config;

	/*Inicializacion del display, leds, switches y sensor*/
	ITSE0803Init(ptr_pines_conf);
	LedsInit();
	SwitchesInit();

	/*configuración ultrasonido*/
	HcSr04Init(GPIO_T_FIL2,GPIO_T_FIL3);

	SwitchActivInt(SWITCH_1, &tecla_1);
	SwitchActivInt(SWITCH_2, &tecla_2);

	serial_config my_uart = {SERIAL_PORT_PC,115200,&doUart};
	UartInit(&my_uart);

	timer_config my_timer = {TIMER_A,1000,&Tiempo};
	TimerInit(&my_timer);
	TimerStart(TIMER_A);
	UartSendString(SERIAL_PORT_PC,"hola mundo");


	while(1)
    {

    }
    return 0;
}

/*==================[end of file]============================================*/

