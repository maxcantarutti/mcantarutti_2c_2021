/*! @mainpage ULTRASOUND METER (SERIAL PORT) 2.0
 * This appliaction shows the distance (in centimeters) between an objet and the sensor, in a display. This uses two devices, an ultrasound meter and a display. Also, the data is shown by HTerm software on PC
 *
 * \section genDesc General Description
 *
 * This application makes the ultrasound meter
 *
 * \section hardConn Hardware Connection
 *
 * |    HC-SR4     	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	T_FIL2		|
 * | 	TRIGGER	 	| 	T_FIL3		|
 * | 	+5V 	 	| 	+5V 		|
 * | 	GND 	 	| 	GND 		|

 *
 * |   ITS_E0803   	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	D1	    	| 	LCD1		|
 * | 	D2	    	| 	LCD2		|
 * | 	D3 	  	    | 	LCD3		|
 * | 	D4  	 	| 	LCD4 		|
 * | 	SEL_0 	 	| 	GPIO1 		|
 * | 	SEL_1 	 	| 	GPIO3 		|
 * | 	SEL_2 	 	| 	GPIO5 		|
 * | 	+5v 	 	| 	+5V 		|
 * | 	GND 	 	| 	GND 		|
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 1/09/2021  | Document creation		                         |
 * | 3/09/2021  | Ultrasound device configurated  and tested     |
 * | 10/09/2021 | New display configuration added and tested     |
 * | 16/09/2021 | Document completed	                         |
 *
 * @author Maximiliano Javier Cantarutti
 *
 */


#ifndef _ULTRASOUND_METER_WI_SERIAL_PORT_H
#define _ULTRASOUND_METER_WI_SERIAL_PORT_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

