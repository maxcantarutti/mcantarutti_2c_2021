/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup Delay
 ** @{ */

/* @brief  EDU-CIAA NXP GPIO driver
 * @author Maximiliano Cantarutti
 *
 * Este driver simula el funcionamiento de una balanza en un sisema de pesaje de cajas
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 11/11/2021 | Document creation	and complete	             |
 *
 */

#ifndef BALANZA_H
#define BALANZA_H


/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup LED Led
 ** @{ */

/** @brief Bare Metal header for leds on EDU-CIAA NXP
 **
 ** This is a driver for six leds mounted on the board
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	MC			Maximiliano Cantarutti


/*****************************************************************************
 * Public macros/types/enumerations/variables definitions
 ****************************************************************************/
#include <stdint.h>

/*****************************************************************************
 * Public functions definitions
 ****************************************************************************/

/**
 * @brief Delay in seconds
 * @param[in] sec seconds to be in delay
 * @return None
 */
void BalanzaInit();

/**

 */
void DatoPeso(uint16_t *peso);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
#endif /* BALANZA_H_ */
