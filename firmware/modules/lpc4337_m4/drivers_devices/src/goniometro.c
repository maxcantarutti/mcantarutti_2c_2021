/* Copyright 2021,

 *Maximiliano Cantarutti
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */



/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup LED Led
 ** @{ */

/** @brief Bare Metal header for leds on EDU-CIAA NXP
 **
 ** This is a driver for six leds mounted on the board
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *  MC			Maximiliano Cantarutti
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 *2021111 Inicio y final
 */

/*==================[inclusions]=============================================*/
#include "goniometro.h"
#include "analog_io.h"






/*==================[macros and definitions]=================================*/

uint16_t angulo_maximo = 75;
uint16_t valor_pote_a_75 = 250 ; /*es el valor obtenido del pote a 75 grados, medidos con un transportador y su valor en binario*/
uint16_t valor_pote;
uint16_t valor_bin;

uint16_t valor_leido;
uint16_t angulo;
/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/
//la siguiente funciion lee y convierte el dato, funcionando de capa intermedia con el driver analog_io
uint16_t ReadDataAndConvertion(){
	AnalogInputRead(CH1, &valor_leido);
	angulo = (valor_leido * angulo_maximo)/valor_pote_a_75;

return angulo;
}

uint16_t PoteValue(){
AnalogInputRead(CH1, &valor_bin);
return(valor_bin);
}




/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/


/*==================[end of file]============================================*/
