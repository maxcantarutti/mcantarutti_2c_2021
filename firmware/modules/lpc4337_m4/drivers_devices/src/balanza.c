/*! @mainpage balanza 1.0
 *
 * \section genDesc General Description
 *
 * Driver para balanza, inicializa el CAD y dependiendo el valor de tension digital
 * devuelve un valor de peso en kg
 *
 * \section hardConn Hardware Connection
 *
 * | 	BALANZA		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	VCC		 	| 	5V			|
 * | 	GND		 	| 	GND			|
 * | 	OUT		 	| 	CH1			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 11/1/2021  | Document creation		                         |
 * | 11/1/2021 	| Document complete	                             |
 *
 * @author Cantarutti Maximiliano
 *
 */
/*==================[inclusions]=============================================*/
#include "balanza.h"
#include "analog_io.h"
#include "bool.h"
#include "chip.h"


/*==================[macros and definitions]=================================*/
#define TENSION0 0.0
#define TENSION_PESO_MAXIMO 5.0
/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/
uint16_t vtension;
uint16_t peso;
uint16_t valor_en_volts;
/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void BalanzaInit()
{	 analog_input_config mi_balanza= {CH1,AINPUTS_SINGLE_READ,NULL};
	AnalogInputInit(&mi_balanza);
	AnalogInputReadPolling(CH1,&vtension);
}

void DatoPeso(uint16_t *peso){
	AnalogInputReadPolling(CH1,&vtension);
	valor_en_volts = (vtension*5)/1023; //se pasa el valor obtenido por analog a voltaje
	if(valor_en_volts<(TENSION0 + 0.2)){ //cuando la tensión sea menor a 0.2V lo tomo como 0
		peso=0;

	}
	if(valor_en_volts>(TENSION_PESO_MAXIMO- 0.2)){ //Cuando la tensión sea mayor a 4,8V lo tomo como peso máximo
		peso=150;
	}
	if((valor_en_volts<TENSION0)&&(vtension>TENSION_PESO_MAXIMO)){ //si esta entre valores, aplico fórmula
		peso = (valor_en_volts/5) * 150; //se comprueba que el valor de tensión sobre 15 por los 150 kilos da valores intermedios de peso y 0 a tension 0
	}

}


/*==================[end of file]============================================*/
